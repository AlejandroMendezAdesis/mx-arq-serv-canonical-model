package com.bbva.utils.google.drive;

import com.bbva.utils.NotNullsLinkedHashSet;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import org.apache.commons.collections.Closure;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.util.IOUtils;

import javax.swing.*;
import java.awt.*;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.ResourceBundle;

/**
 * Created by Alejandro on 25/11/2014.
 */
public class GoogleDriveUtils {

    private static Drive service;
    private static String REDIRECT_URI = "urn:ietf:wg:oauth:2.0:oob";

    public GoogleDriveUtils() throws IOException {
        ResourceBundle bundle = ResourceBundle.getBundle("mx-arq-serv-canonical-model");
        String clientId = bundle.getString("clientId");
        String clientSecret = bundle.getString("clientSecret");
        init(clientId, clientSecret);
    }

    public GoogleDriveUtils(final String clientId, final String clientSecret) throws IOException {
        init(clientId, clientSecret);
    }

    private static void init(final String clientId, final String clientSecret) throws IOException {
        if (service == null) {
            final HttpTransport httpTransport = new NetHttpTransport();
            final JsonFactory jsonFactory = new JacksonFactory();

            final GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(httpTransport, jsonFactory, clientId,
                    clientSecret, Arrays.asList(DriveScopes.DRIVE)).setAccessType("online").setApprovalPrompt("auto").build();

            GoogleTokenResponse response;

            final String url = flow.newAuthorizationUrl().setRedirectUri(REDIRECT_URI).build();
            System.out.println("Please open the following URL in your browser then type the authorization code:");
            System.out.println("  " + url);
            try {
                java.awt.Desktop.getDesktop().browse(new URI(url));
            } catch (final URISyntaxException e) {
                e.printStackTrace();
            }

            response = flow.newTokenRequest(showRetrieveCodePanel()).setRedirectUri(REDIRECT_URI).execute();

            final GoogleCredential credential = new GoogleCredential().setFromTokenResponse(response);
            // Create a new authorized API client
            service = new Drive.Builder(httpTransport, jsonFactory, credential).build();
        }

    }

    private static String showRetrieveCodePanel() {
        final JFrame frame = new JFrame();
        final JPanel panel = new JPanel(new GridLayout(0, 1));
        final JTextField field1 = new JTextField("");
        panel.add(new JLabel("Please enter authorization code from Google Drive:"));
        panel.add(field1);
        final int result = JOptionPane.showConfirmDialog(null, panel, "Test", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (result == JOptionPane.OK_OPTION) {
            System.out.println("Code entered:  " + field1.getText());

        } else {
            System.out.println("Cancelled");
        }
        frame.add(panel);
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                frame.toFront();
                frame.repaint();
            }
        });
        return field1.getText();
    }

    private static String showRetrieveFileIdPanel() {
        final JFrame frame = new JFrame();
        final JPanel panel = new JPanel(new GridLayout(0, 1));
        final JTextField field1 = new JTextField("");
        panel.add(new JLabel("Please enter the id of the file to inspect from Google Drive:"));
        panel.add(field1);
        final int result = JOptionPane.showConfirmDialog(null, panel, "Test", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (result == JOptionPane.OK_OPTION) {
            System.out.println("Id entered:  " + field1.getText());

        } else {
            System.out.println("Cancelled");
        }
        frame.add(panel);
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                frame.toFront();
                frame.repaint();
            }
        });
        return field1.getText();
    }

    /**
     * Print a file's metadata.
     *
     * @param fileId ID of the file to print metadata for.
     */
    public File getFileById(final String fileId) {

        try {
            final File file = service.files().get(fileId).execute();

            System.out.println("Title: " + file.getTitle());
            System.out.println("Description: " + file.getDescription());
            System.out.println("MIME type: " + file.getMimeType());
            return file;
        } catch (final IOException e) {
            System.out.println("An error occured: " + e);
        }
        return null;
    }

    /**
     * Download a file's content.
     *
     * @param file Drive File instance.
     * @return InputStream containing the file's content if successful,
     * {@code null} otherwise.
     */
    public InputStream downloadInputStreamFromDrive(final File file) {
        final String downloadUrl = file.getExportLinks().get("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        if (downloadUrl != null && downloadUrl.length() > 0) {
            try {
                final HttpResponse resp = service.getRequestFactory().buildGetRequest(new GenericUrl(downloadUrl)).execute();
                return resp.getContent();
            } catch (final IOException e) {
                // An error occurred.
                e.printStackTrace();
                return null;
            }
        } else {
            // The file doesn't have any content stored on Drive.
            return null;
        }
    }

    public void downloadFileFromDrive(final File file, final java.io.File targetFile) throws IOException {
        final InputStream inputStream = downloadInputStreamFromDrive(file);

        final OutputStream outStream = new FileOutputStream(targetFile);

        final byte[] buffer = new byte[8 * 1024];
        int bytesRead;
        while ((bytesRead = inputStream.read(buffer)) != -1) {
            outStream.write(buffer, 0, bytesRead);
        }
        IOUtils.closeQuietly(inputStream);
        IOUtils.closeQuietly(outStream);

    }

    public Collection<java.io.File> downloadFilesFromDrive() throws IOException {
        final Collection<java.io.File> targetFiles = new ArrayList<java.io.File>();
        ResourceBundle bundle = ResourceBundle.getBundle("mx-arq-serv-canonical-model");
        String fieldIds = bundle.getString("filesIds");
        fieldIds = StringUtils.isBlank(fieldIds) ? showRetrieveFileIdPanel() : fieldIds;
        fieldIds = fieldIds.replaceAll(";", "\n").replaceAll(" ", "\n").replaceAll(",", "\n");
        final String[] fieldIdsFixed = StringUtils.split(fieldIds, "\n");
        final NotNullsLinkedHashSet files = new NotNullsLinkedHashSet(CollectionUtils.select(Arrays.asList(fieldIdsFixed), new Predicate() {
            public boolean evaluate(final Object o) {
                return o != null && StringUtils.isNotBlank(((String) o).trim());
            }
        }));
        CollectionUtils.forAllDo(files, new Closure() {
            public void execute(final Object o) {
                final File file = getFileById((String) o);
                try {
                    final java.io.File targetFile = java.io.File.createTempFile(file.getTitle() + "-", ".xlsx");
                    downloadFileFromDrive(file, targetFile);
                    targetFiles.add(targetFile);
                } catch (final IOException e) {
                    e.printStackTrace();
                }

            }
        });
        return targetFiles;
    }
}