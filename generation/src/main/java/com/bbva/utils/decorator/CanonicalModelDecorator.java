package com.bbva.utils.decorator;

import com.bbva.utils.AttributesUtils;
import com.bbva.utils.JavaClassGenerator;
import com.bbva.utils.templates.model.canonical.Attributes;
import com.bbva.utils.templates.model.canonical.CanonicalModel;
import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Alejandro on 22/12/2014.
 */
public class CanonicalModelDecorator {

    private final String origin;
    public static final String CANONICAL_PREFIX = "(MCD)(.*):(.*)|(mcd)(.*):(.*)|(Mcd)(.*):(.*)";

    public CanonicalModelDecorator(String origin) {
        this.origin = origin;
    }

    public String decorateEntity(CanonicalModel canonicalModel) {
        return canonicalModel.getEntity()
                .replaceAll("-", StringUtils.EMPTY)
                .replaceAll("_", StringUtils.EMPTY)
                .replaceAll(" ", StringUtils.EMPTY)
                .replaceAll("\t", StringUtils.EMPTY)
                .replaceAll("\n", StringUtils.EMPTY)
                .replaceAll("á", "a")
                .replaceAll("é", "e")
                .replaceAll("í", "i")
                .replaceAll("ó", "o")
                .replaceAll("ú", "u")
                .replaceAll("ñ", "n")
                .replaceAll("Á", "A")
                .replaceAll("É", "E")
                .replaceAll("Í", "I")
                .replaceAll("Ó", "O")
                .replaceAll("Ú", "U")
                .replaceAll("Ñ", "N");
    }

    public String decorateAttributeName(Attributes attribute) {
        return StringUtils.uncapitalize(attribute.getAttribute()).trim().replaceAll("@", StringUtils.EMPTY);
    }

    public String decorateFullyQualifiedAttributeType(Attributes attribute, boolean considerLists) throws ClassNotFoundException {
        return decorateFullyQualifiedType(attribute.getType(), considerLists);
    }

    public String decorateFullyQualifiedType(String type, boolean considerLists) throws ClassNotFoundException {
        String importType = StringUtils.capitalize(type);
        String result = AttributesUtils.getJavaTypeOrNull(type, considerLists);
        if (StringUtils.isNotBlank(result)) {
            return result;
        }
        if (considerLists) {
            if (AttributesUtils.isList(type)) {
                return "java.util.List<" + JavaClassGenerator.CANONICAL_MODEL_PACKAGE_NAME_BASE + "." + origin + "." + AttributesUtils.getExtractedGenerics(importType) + ">";
            }
        }
        return JavaClassGenerator.CANONICAL_MODEL_PACKAGE_NAME_BASE + "." + origin + "." + importType;
    }

    public String getCanonicalPrefixPatternForOrigin() {
        return "(MCD.*" + origin + ":)(.*)|(mcd.*" + origin + ":)(.*)|(Mcd.*" + origin + ":)(.*)";
    }

    public String unDecorateAttributeType(String attributeType) {
        Pattern pattern = Pattern.compile(CANONICAL_PREFIX);
        Matcher matcher = pattern.matcher(attributeType);
        if (matcher.find()) {
            final String group = matcher.group(3);
            return StringUtils.defaultString(group, "").trim();
        } else {
            return attributeType;
        }
    }


    public String unDecorateCanonicalOrigin(String attributeType) {
        Pattern pattern = Pattern.compile(CANONICAL_PREFIX);
        Matcher matcher = pattern.matcher(attributeType);
        if (matcher.find()) {
            final String group = matcher.group(2);
            return StringUtils.defaultString(group, "").trim();
        } else {
            return attributeType;
        }
    }

    public boolean isCanonicalReference(String attributeType) {
        return !unDecorateAttributeType(attributeType).equals(attributeType) && !unDecorateCanonicalOrigin(attributeType).equals(attributeType) ;
    }

}
