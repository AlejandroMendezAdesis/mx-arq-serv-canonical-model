package com.bbva.utils.decorator;

import com.bbva.utils.templates.model.canonical.CanonicalModels;
import com.bbva.utils.templates.model.xls.Origin;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by Alejandro on 22/12/2014.
 */
public class CanonicalModelsDecorator {
    public static String decorateOrigin(CanonicalModels canonicalModels) {
        Origin origin = canonicalModels.getOrigin();
        return origin.getCountryName()
                .replaceAll("MCD", StringUtils.EMPTY)
                .replaceAll("-", StringUtils.EMPTY)
                .replaceAll("_", StringUtils.EMPTY)
                .replaceAll(" ", StringUtils.EMPTY)
                .replaceAll("á", "a")
                .replaceAll("é", "e")
                .replaceAll("í", "i")
                .replaceAll("ó", "o")
                .replaceAll("ú", "u")
                .replaceAll("ñ", "n")
                .toLowerCase();
    }

}
