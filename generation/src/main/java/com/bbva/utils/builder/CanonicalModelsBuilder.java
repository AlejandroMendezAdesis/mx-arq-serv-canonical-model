package com.bbva.utils.builder;

import com.bbva.utils.NotNullsLinkedHashSet;
import com.bbva.utils.templates.model.canonical.Attributes;
import com.bbva.utils.templates.model.canonical.CanonicalModel;
import com.bbva.utils.templates.model.canonical.CanonicalModels;
import com.bbva.utils.templates.model.xls.CanonicalModelTemplate;
import com.bbva.utils.templates.model.xls.Origin;
import org.apache.commons.lang3.StringUtils;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by Alejandro on 19/12/2014.
 */
public class CanonicalModelsBuilder {
    public static CanonicalModels build(String originName, NotNullsLinkedHashSet<CanonicalModelTemplate> canonicalModelList) throws Exception {
        Origin origin = new Origin();
        origin.setCountryName(originName);
        CanonicalModels canonicalModels = new CanonicalModels();
        canonicalModels.setOrigin(origin);
        final NotNullsLinkedHashSet<CanonicalModelTemplate> listAttributesFixed = new NotNullsLinkedHashSet<CanonicalModelTemplate>(true);
        String entityName = StringUtils.EMPTY;
        for (CanonicalModelTemplate canonicalModelTemplate : canonicalModelList) {
            if (StringUtils.isBlank(canonicalModelTemplate.getEntity()) && StringUtils.isBlank(canonicalModelTemplate.getAttribute())
                    && StringUtils.isBlank(canonicalModelTemplate.getFormat()) && StringUtils.isBlank(canonicalModelTemplate.getType())
                    && StringUtils.isBlank(canonicalModelTemplate.getExtendsImplements()) && StringUtils.isBlank(canonicalModelTemplate.getDescription())) {
                continue;
            }
            if (StringUtils.isNotBlank(canonicalModelTemplate.getEntity()) && StringUtils.isBlank(canonicalModelTemplate.getAttribute())
                    && StringUtils.isBlank(canonicalModelTemplate.getFormat()) && StringUtils.isBlank(canonicalModelTemplate.getType())
                    && StringUtils.isBlank(canonicalModelTemplate.getExtendsImplements()) && StringUtils.isBlank(canonicalModelTemplate.getDescription())) {
                entityName = canonicalModelTemplate.getEntity();
            } else {
                if (StringUtils.isNotBlank(entityName) && StringUtils.isBlank(canonicalModelTemplate.getEntity())) {
                    canonicalModelTemplate.setEntity(entityName);
                } else if (StringUtils.isBlank(entityName) && StringUtils.isNotBlank(canonicalModelTemplate.getEntity())) {
                    entityName = canonicalModelTemplate.getEntity();
                } else if (StringUtils.isBlank(entityName) && StringUtils.isBlank(canonicalModelTemplate.getEntity())) {
                    throw new IllegalStateException("Una entidad debe tener un nombre obligatoriamente");
                } else if (StringUtils.isNotBlank(entityName) && StringUtils.isNotBlank(canonicalModelTemplate.getEntity())
                        && !entityName.equals(canonicalModelTemplate.getEntity())) {
                    entityName = canonicalModelTemplate.getEntity();
                }
                listAttributesFixed.add(canonicalModelTemplate);
            }
        }
        NotNullsLinkedHashSet<Attributes> listAttributesPerCanonicalModel = new NotNullsLinkedHashSet<Attributes>(true);
        entityName = StringUtils.EMPTY;
        CanonicalModel canonicalModel = new CanonicalModel();
        Set<CanonicalModel> models = new LinkedHashSet<CanonicalModel>();
        for (CanonicalModelTemplate canonicalModelTemplate : listAttributesFixed) {
            if (StringUtils.isBlank(canonicalModelTemplate.getAttribute())
                    && ("String".equalsIgnoreCase(canonicalModelTemplate.getType())
                    || "java.lang.String".equalsIgnoreCase(canonicalModelTemplate.getType())
                    || "enum".equalsIgnoreCase(canonicalModelTemplate.getType()))) {
                CanonicalModel canonicalModel_ = new CanonicalModel();
                Attributes attributes = new Attributes();
                attributes.setAttribute(StringUtils.isNotBlank(canonicalModelTemplate.getAttribute()) ? canonicalModelTemplate.getAttribute().trim() : null);
                attributes.setType("enum");
                attributes.setDescription(StringUtils.isNotBlank(canonicalModelTemplate.getDescription()) ? canonicalModelTemplate.getDescription().trim() : null);
                canonicalModel_.getAttributes().add(attributes);
                canonicalModel_.setEntity(canonicalModelTemplate.getEntity().trim());
                models.add(canonicalModel_);
                entityName = StringUtils.EMPTY;
            } else if (StringUtils.isBlank(canonicalModelTemplate.getAttribute())
                    && StringUtils.isBlank(canonicalModelTemplate.getType())) {
                CanonicalModel canonicalModel_ = new CanonicalModel();
                Attributes attributes = new Attributes();
                attributes.setAttribute(StringUtils.isNotBlank(canonicalModelTemplate.getAttribute()) ? canonicalModelTemplate.getAttribute().trim() : null);
                attributes.setType("interface");
                attributes.setDescription(StringUtils.isNotBlank(canonicalModelTemplate.getDescription()) ? canonicalModelTemplate.getDescription().trim() : null);
                canonicalModel_.getAttributes().add(attributes);
                canonicalModel_.setEntity(canonicalModelTemplate.getEntity().trim());
                canonicalModel_.setExtendsImplements(StringUtils.isNotBlank(canonicalModelTemplate.getExtendsImplements()) ? canonicalModelTemplate.getExtendsImplements().trim() : null);
                models.add(canonicalModel_);
                entityName = StringUtils.EMPTY;
            } else if (entityName.equals(canonicalModelTemplate.getEntity())) {
                Attributes attributes = new Attributes();
                if (StringUtils.isBlank(canonicalModelTemplate.getType())) {
                    throw new Exception("There is field named '" + canonicalModelTemplate.getAttribute() + "' on entity '"
                            + canonicalModelTemplate.getEntity() + "' with an empty type, please fix it an then run this program again");
                }
                if (canonicalModelTemplate.getAttribute().contains("[")) {
                    String newAttribute = canonicalModelTemplate.getAttribute().split("\\[")[0];
                    attributes.setAttribute(newAttribute.trim());
                    attributes.setType("List<" + canonicalModelTemplate.getType().trim() + ">");
                } else {
                    attributes.setAttribute(canonicalModelTemplate.getAttribute().trim());
                    attributes.setType(canonicalModelTemplate.getType().trim());
                }
                attributes.setFormat(StringUtils.isNotBlank(canonicalModelTemplate.getFormat()) ? canonicalModelTemplate.getFormat().trim() : null);
                attributes.setDescription(StringUtils.isNotBlank(canonicalModelTemplate.getDescription()) ? canonicalModelTemplate.getDescription().trim() : null);
                listAttributesPerCanonicalModel.add(attributes);
            } else {
                if (listAttributesPerCanonicalModel.size() > 0) {
                    canonicalModel.getAttributes().addAll(listAttributesPerCanonicalModel);
                    listAttributesPerCanonicalModel = new NotNullsLinkedHashSet<Attributes>(true);
                }
                canonicalModel = new CanonicalModel();
                Attributes attributes = new Attributes();
                if (StringUtils.isBlank(canonicalModelTemplate.getType())) {
                    throw new Exception("There is field named '" + canonicalModelTemplate.getAttribute() + "' on entity '"
                            + canonicalModelTemplate.getEntity() + "' with an empty type, please fix it an then run this program again");
                }
                if (canonicalModelTemplate.getAttribute().contains("[")) {
                    String newAttribute = canonicalModelTemplate.getAttribute().split("\\[")[0];
                    attributes.setAttribute(newAttribute.trim());
                    attributes.setType("List<" + canonicalModelTemplate.getType().trim() + ">");
                } else {
                    attributes.setAttribute(canonicalModelTemplate.getAttribute().trim());
                    attributes.setType(canonicalModelTemplate.getType().trim());
                }
                attributes.setFormat(StringUtils.isNotBlank(canonicalModelTemplate.getFormat()) ? canonicalModelTemplate.getFormat().trim() : null);
                attributes.setDescription(StringUtils.isNotBlank(canonicalModelTemplate.getDescription()) ? canonicalModelTemplate.getDescription().trim() : null);
                canonicalModel.getAttributes().add(attributes);
                canonicalModel.setExtendsImplements(StringUtils.isNotBlank(canonicalModelTemplate.getExtendsImplements()) ? canonicalModelTemplate.getExtendsImplements().trim() : null);
                canonicalModel.setEntity(canonicalModelTemplate.getEntity().trim());
                entityName = canonicalModel.getEntity();
                models.add(canonicalModel);
            }
        }
        if (models.size() > 0) {
            canonicalModel.getAttributes().addAll(listAttributesPerCanonicalModel);
        }

        canonicalModels.setModels(models);
        return canonicalModels;
    }
}
