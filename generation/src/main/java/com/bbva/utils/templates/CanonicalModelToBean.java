package com.bbva.utils.templates;

import com.bbva.utils.NotNullsLinkedHashSet;
import com.bbva.utils.builder.CanonicalModelsBuilder;
import com.bbva.utils.templates.model.canonical.CanonicalModels;
import com.bbva.utils.templates.model.xls.CanonicalModelTemplate;
import net.sf.jxls.reader.ReaderBuilder;
import net.sf.jxls.reader.XLSReader;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.*;
import java.util.*;

/**
 * Created by Alejandro on 20/11/2014.
 */

public class CanonicalModelToBean {

    /* The excel data file to parse */
    private final File xlsFile;
    /* The jxls config file describing how to map rows to beans */
    private final File jxlsCanonicalModelConfigFile;
    /* The jxls config file describing how to map rows to beans */
    private final File jxlsCanonicalModelEspanaConfigFile;

    private static final String TAB_NAME_PATTERN = "---TAB_NAME_REPLACE---";
    private static ResourceBundle bundle = ResourceBundle.getBundle("mx-arq-serv-canonical-model");

    private final Collection<String> CANONICAL_MODEL_SERVICES_TAB_EXCLUSION_LIST = new ArrayList<String>() {

        private static final long serialVersionUID = 4847435515923442316L;

        {
            addAll(Arrays.asList(bundle.getString("tabExclusions").split(",")));
            add(bundle.getString("tabEspañaCanonicalModelName"));
        }
    };

    public CanonicalModelToBean(File xlsFile, File jxlsCanonicalModelConfigFile, File jxlsCanonicalModelEspanaConfigFile) {
        this.xlsFile = xlsFile;
        this.jxlsCanonicalModelConfigFile = jxlsCanonicalModelConfigFile;
        this.jxlsCanonicalModelEspanaConfigFile = jxlsCanonicalModelEspanaConfigFile;
    }

    /**
     * Parses an excel file into a list of beans.
     *
     * @return a of bean of CanonicalModel or empty
     * if there are none
     * @throws Exception if there is a problem parsing the file
     */
    public Collection<CanonicalModels> parseExcelFile() throws Throwable {
        final NotNullsLinkedHashSet<CanonicalModels> canonicalModelsList = new NotNullsLinkedHashSet<CanonicalModels>();
        final Workbook workbook = WorkbookFactory.create(xlsFile);
        for (int sheetNo = 0; sheetNo < workbook.getNumberOfSheets(); sheetNo++) {
            final String sheetName = workbook.getSheetName(sheetNo).trim();
            XLSReader xlsReader = null;
            if (!CANONICAL_MODEL_SERVICES_TAB_EXCLUSION_LIST.contains(sheetName)) {
                xlsReader = ReaderBuilder.buildFromXML(setTabNameToConfigFile(jxlsCanonicalModelConfigFile, sheetName));
            } else if (sheetName.equals(bundle.getString("tabEspañaCanonicalModelName"))) {
                xlsReader = ReaderBuilder.buildFromXML(setTabNameToConfigFile(jxlsCanonicalModelEspanaConfigFile, sheetName));
            }
            if (xlsReader != null) {
                Map<String, Object> beans = new HashMap<String, Object>();
                NotNullsLinkedHashSet<CanonicalModelTemplate> canonicalModels = new NotNullsLinkedHashSet<CanonicalModelTemplate>();
                beans.put("emptyCollection1", new NotNullsLinkedHashSet<String>());
                beans.put("canonicalModels", canonicalModels);
                try {
                    InputStream inputStream = new BufferedInputStream(new FileInputStream(xlsFile));
                    xlsReader.read(inputStream, beans);
                    canonicalModelsList.add(CanonicalModelsBuilder.build(sheetName, canonicalModels));
                } catch (final Throwable t) {
                    System.out.println("File '" + xlsFile.getName() + "' couldn't be parsed because of the following exception: " + t.getMessage());
                    throw t;
                }
            }
        }

        return canonicalModelsList;
    }

    private File setTabNameToConfigFile(File configFile, final String sheetName) throws IOException {
        final File jxlsCanonicalModelConfigFileCloned = File.createTempFile(
                configFile.getName() + "-", ".xml");
        FileUtils.copyFile(configFile, jxlsCanonicalModelConfigFileCloned);
        String content = IOUtils.toString(new FileInputStream(jxlsCanonicalModelConfigFileCloned), "UTF-8");
        content = content.replaceAll(TAB_NAME_PATTERN, sheetName);
        IOUtils.write(content, new FileOutputStream(jxlsCanonicalModelConfigFileCloned), "UTF-8");
        return jxlsCanonicalModelConfigFileCloned;
    }
}
