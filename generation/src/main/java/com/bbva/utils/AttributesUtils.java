package com.bbva.utils;

import com.bbva.utils.templates.model.canonical.Attributes;
import com.bbva.utils.templates.model.canonical.CanonicalModel;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Alejandro on 24/12/2014.
 */
public class AttributesUtils {

    public static Collection<String> COMMONS_TYPES_PREFIXES = new ArrayList<String>() {
        {
            add("java.lang");
            add("java.util");
            add("java.math");
            add("java.io");
            add("java.sql");
            add("java.text");
            add("java.net");
            add("org.joda.time");
        }
    };

    public static boolean isList(String type) {
        return type.startsWith("List<") || type.startsWith("java.lang.List<");
    }

    public static String getExtractedGenerics(String s) {
        return s.split("List<")[1].replaceAll(">", StringUtils.EMPTY);
    }

    public static boolean isInterface(String type) {
        return "interface".equalsIgnoreCase(type);
    }

    public static String getJavaTypeOrNull(String type, boolean considerLists) {
        String capitalizedType = StringUtils.capitalize(type);
        Class clazz;
        if (considerLists) {
            if (AttributesUtils.isList(type)) {
                String generics = getExtractedGenerics(type);
                final String javaType = getJavaTypeOrNull(generics, false);
                if (StringUtils.isBlank(javaType)) {
                    return null;
                } else {
                    return "java.util.List<" + javaType + ">";
                }
            }
        }
        for (String javaTypePrefix : AttributesUtils.COMMONS_TYPES_PREFIXES) {
            try {
                clazz = Class.forName(javaTypePrefix + "." + capitalizedType);
                if (considerLists) {
                    if (AttributesUtils.isList(type)) {
                        return "java.util.List<" + clazz.getName() + ">";
                    }
                }
                return clazz.getName();
            } catch (ClassNotFoundException ignored) {

            }
        }
        try {
            Method method = Class.class.getDeclaredMethod("getPrimitiveClass", String.class);
            method.setAccessible(true);
            clazz = (Class) method.invoke(Class.forName("java.lang.Class"), StringUtils.uncapitalize(type));
            if (considerLists) {
                if (AttributesUtils.isList(type)) {
                    return "java.util.List<" + clazz.getName() + ">";
                }
            }
            return clazz.getName();
        } catch (InvocationTargetException | ClassNotFoundException | NoSuchMethodException | IllegalAccessException | NullPointerException ignored) {
        }
        return null;
    }

    public static boolean isAttributeAnEnumerated(Collection<CanonicalModel> canonicalModels, final Attributes attribute) throws ClassNotFoundException {
        final String type = attribute.getType();
        CanonicalModel canonicalModel = (CanonicalModel) org.apache.commons.collections.CollectionUtils.find(canonicalModels, new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                return type.equals(((CanonicalModel) object).getEntity());
            }
        });
        if (canonicalModel != null) {
            return isEntityAnEnumeration(canonicalModel);
        } else {
            if (isList(type)) {
                return false;
            } else {
                final String javaType = getJavaTypeOrNull(type, false);
                return javaType != null && Enum.class.isAssignableFrom(Class.forName(javaType).getClass());
            }
        }
    }


    public static boolean isEntityAnEnumeration(CanonicalModel canonicalModel) {
        return canonicalModel.getAttributes().size() == 1 && canonicalModel.getAttributes().iterator().next().getType().equals("enum");
    }

    public static boolean isEntityAnInterface(CanonicalModel canonicalModel) {
        final Attributes attributes = canonicalModel.getAttributes().iterator().next();
        return canonicalModel.getAttributes().size() == 1 && "interface".equals(attributes.getType()) && StringUtils.isBlank(attributes.getAttribute());
    }

}
