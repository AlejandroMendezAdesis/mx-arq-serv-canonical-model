package com.bbva.utils;

import com.bbva.utils.decorator.CanonicalModelDecorator;
import com.bbva.utils.decorator.CanonicalModelsDecorator;
import com.bbva.utils.templates.model.canonical.Attributes;
import com.bbva.utils.templates.model.canonical.CanonicalModel;
import com.bbva.utils.templates.model.canonical.CanonicalModels;
import com.strobel.assembler.metadata.ClasspathTypeLoader;
import com.strobel.assembler.metadata.ITypeLoader;
import com.strobel.decompiler.Decompiler;
import com.strobel.decompiler.DecompilerSettings;
import com.strobel.decompiler.PlainTextOutput;
import com.strobel.decompiler.languages.java.JavaFormattingOptions;
import javassist.*;
import javassist.bytecode.DuplicateMemberException;
import org.apache.commons.collections.Predicate;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.net.URLDecoder;
import java.util.Collection;

/**
 * Created by Alejandro on 22/12/2014.
 */
public class JavaClassGenerator {

    public static final String CANONICAL_MODEL_PACKAGE_NAME_BASE = "com.bbva.model.canonical";
    final static DecompilerSettings settings = javaDefaults();
    private static CanonicalModelDecorator canonicalModelDecorator;

    static {
        ITypeLoader typeLoader = new ClasspathTypeLoader();
        settings.setTypeLoader(typeLoader);
    }

    /**
     * @param canonicalModelProjectBasePath
     * @param canonicalModels
     * @throws Exception
     */
    public static void generateJavaClassFromCanonicalModel(File canonicalModelProjectBasePath, CanonicalModels canonicalModels) throws Exception {

        final String origin = CanonicalModelsDecorator.decorateOrigin(canonicalModels);
        String javaCanonicalModelSourcePackage = CANONICAL_MODEL_PACKAGE_NAME_BASE + "." + origin;
        canonicalModelDecorator = new CanonicalModelDecorator(origin);
        String javaCanonicalModelPath = (canonicalModelProjectBasePath.isDirectory()
                ? canonicalModelProjectBasePath.getCanonicalFile().getAbsolutePath()
                : canonicalModelProjectBasePath.getParentFile().getCanonicalFile().getAbsolutePath());
        javaCanonicalModelPath = javaCanonicalModelPath.replaceAll("\\\\", "/");
        File file = new File(javaCanonicalModelPath + "/"
                + javaCanonicalModelSourcePackage.replaceAll("\\.", "/"));
        String classesBasePath = new File(URLDecoder.decode(JavaClassGenerator.class.getResource("/").getFile(), "UTF-8") + "/../classes").getCanonicalFile().getAbsolutePath();
        final File javaCanonicalModelSourcePackageFile = new File(classesBasePath + "/" + javaCanonicalModelSourcePackage.replaceAll("\\.", "/"));
        try {
            FileUtils.forceDelete(file);
        } catch (Throwable ignored) {
        }
        try {
            FileUtils.forceDelete(javaCanonicalModelSourcePackageFile);
        } catch (Throwable ignored) {
        }
        try {
            FileUtils.forceMkdir(file);
        } catch (Throwable t) {
            t.printStackTrace();
        }
        Collection<CanonicalModel> canonicalModelsList = canonicalModels.getModels();

        generateJavaClasses(javaCanonicalModelSourcePackage, javaCanonicalModelPath, canonicalModelsList, classesBasePath);
    }

    private static void generateJavaClasses(String javaCanonicalModelSourcePackage, String javaCanonicalModelPath, Collection<CanonicalModel> canonicalModelsList, String classesBasePath) throws Exception {
        for (CanonicalModel canonicalModel : canonicalModelsList) {
            generateJavaClass(canonicalModelsList, javaCanonicalModelSourcePackage, javaCanonicalModelPath, classesBasePath, canonicalModel);
        }
    }

    private static void generateJavaClass(Collection<CanonicalModel> canonicalModels, String javaCanonicalModelSourcePackage, String javaCanonicalModelPath, String classesBasePath, CanonicalModel canonicalModel) throws Exception {
        ClassPool pool = ClassPool.getDefault();
        String className = StringUtils.capitalize(canonicalModelDecorator.decorateEntity(canonicalModel));
        String fullyQulifiedClassName = javaCanonicalModelSourcePackage + "." + className;
        canonicalModel.setEnumerate(AttributesUtils.isEntityAnEnumeration(canonicalModel));
        canonicalModel.setInterface(AttributesUtils.isEntityAnInterface(canonicalModel));
        CtClass cc;
        if (canonicalModel.isInterface()) {
            cc = pool.makeInterface(fullyQulifiedClassName);
        } else {
            cc = pool.makeClass(fullyQulifiedClassName);
        }
        final String extendsImplements = canonicalModel.getExtendsImplements();
        if (StringUtils.isNotBlank(extendsImplements)) {
            String fullyQulifiedExtendsImplements = canonicalModelDecorator.decorateFullyQualifiedType(extendsImplements, false);
            if (fullyQulifiedExtendsImplements != null) {
                CanonicalModel canonicalModel_ = (CanonicalModel) org.apache.commons.collections.CollectionUtils.find(canonicalModels, new Predicate() {
                    @Override
                    public boolean evaluate(Object object) {
                        return ((CanonicalModel) object).getEntity().equals(extendsImplements);
                    }
                });
                if (canonicalModel_ != null) {
                    if (canonicalModel_.isInterface()) {
                        cc.addInterface(pool.get(fullyQulifiedExtendsImplements));
                    } else if (!canonicalModel_.isEnumerate()) {
                        cc.setSuperclass(pool.get(fullyQulifiedExtendsImplements));
                    }
                }
            }
        }

        String javaFileName = javaCanonicalModelPath + "/" + javaCanonicalModelSourcePackage.replaceAll("\\.", "/") + "/" + className;
        String internalName = fullyQulifiedClassName.replace("\\.class", StringUtils.EMPTY).replaceAll("\\.", "/");

        generateJavaClass(canonicalModels, javaCanonicalModelSourcePackage, javaCanonicalModelPath, classesBasePath, canonicalModel, pool, className, fullyQulifiedClassName, cc, javaFileName, internalName);

    }

    private static void generateJavaClass(Collection<CanonicalModel> canonicalModels, String javaCanonicalModelSourcePackage, String javaCanonicalModelPath, String classesBasePath, CanonicalModel canonicalModel, ClassPool pool, String className, String fullyQulifiedClassName, CtClass cc, String javaFileName, String internalName) throws Exception {
        if (!AttributesUtils.isEntityAnEnumeration(canonicalModel)) {
            addFieldsRecursively(canonicalModels, canonicalModel, cc, pool, javaCanonicalModelSourcePackage, javaCanonicalModelPath, classesBasePath);
            addGettersAndSetters(canonicalModels, canonicalModel, cc);
        }
        final String completeJavaFileName = javaFileName + ".java";
        try {
            cc.writeFile(classesBasePath);
            try {
                try (FileOutputStream stream = new FileOutputStream(completeJavaFileName)) {

                    try (OutputStreamWriter writer = new OutputStreamWriter(stream)) {
                        Decompiler.decompile(
                                internalName,
                                new PlainTextOutput(writer),
                                settings
                        );

                    } catch (Throwable t) {
                        t.printStackTrace();
                    }
                }
            } catch (final IOException e) {
                e.printStackTrace();
            }
        } catch (CannotCompileException e) {
            e.printStackTrace();
        }
        if (AttributesUtils.isEntityAnEnumeration(canonicalModel)) {
            fixEnumeratedJavaFile(completeJavaFileName);
        }
    }

    private static void fixEnumeratedJavaFile(String completeJavaFileName) {

        String tmpCompleteJavaFileName = completeJavaFileName + "_tmp";

        BufferedReader br = null;
        BufferedWriter bw = null;
        try {
            br = new BufferedReader(new FileReader(completeJavaFileName));
            bw = new BufferedWriter(new FileWriter(tmpCompleteJavaFileName));
            String line;
            while ((line = br.readLine()) != null) {
                if (line.contains("public class")) {
                    line = line.replace("public class", "public enum");
                }
                if (line.contains(" extends Enum")) {
                    line = line.replace(" extends Enum", StringUtils.EMPTY);
                }
                bw.write(line + "\n");
            }
        } catch (Exception e) {
            return;
        } finally {
            try {
                if (br != null)
                    br.close();
            } catch (IOException e) {
                //
            }
            try {
                if (bw != null)
                    bw.close();
            } catch (IOException e) {
                //
            }
        }
        // Once everything is complete, delete old file..
        File oldFile = new File(completeJavaFileName);
        oldFile.delete();

        // And rename tmp file's name to old file name
        File newFile = new File(tmpCompleteJavaFileName);
        newFile.renameTo(oldFile);

    }

    private static void addFieldsRecursively(Collection<CanonicalModel> canonicalModels, CanonicalModel canonicalModel, CtClass cc, ClassPool pool, String javaCanonicalModelSourcePackage, String javaCanonicalModelPath, String classesBasePath) throws Exception {
        for (Attributes attribute : canonicalModel.getAttributes()) {
            try {
                addField(cc, pool, attribute);
            } catch (DuplicateMemberException dme) {
                throw new Exception("There is a duplicated attribute named '" + attribute.getAttribute() + "' on entity '"
                        + canonicalModel.getEntity() + "', please fix it an then run this program again");
            } catch (CannotCompileException cce) {
                addRecursiveField(canonicalModels, canonicalModel, attribute, javaCanonicalModelSourcePackage, javaCanonicalModelPath, classesBasePath);
                String classname;
                if (!canonicalModelDecorator.isCanonicalReference(attribute.getType())) {
                    if (AttributesUtils.isList(attribute.getType())) {
                        Attributes attributes_ = new Attributes();
                        attributes_.setType(AttributesUtils.getExtractedGenerics(attribute.getType()));
                        classname = canonicalModelDecorator.decorateFullyQualifiedAttributeType(attributes_, false);
                    } else {
                        classname = canonicalModelDecorator.decorateFullyQualifiedAttributeType(attribute, false);
                    }
                    Loader cl = new Loader(pool);
                    CtClass newCreatedClass = pool.get(classname);
                    cl.loadClass(classname);
                    newCreatedClass.defrost();
                    addField(cc, pool, attribute);
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private static CtMethod generateGetter(CtClass declaringClass, Attributes attribute)
            throws CannotCompileException, ClassNotFoundException {
        String fieldName = attribute.getAttribute();
        String fieldClass = attribute.getType().trim();
        if (!canonicalModelDecorator.isCanonicalReference(attribute.getType())) {
            if (fieldName != null) {
                fieldName = canonicalModelDecorator.decorateAttributeName(attribute);
                if (AttributesUtils.isList(fieldClass)) {
                    fieldClass = "java.util.List";
                } else {
                    fieldClass = canonicalModelDecorator.decorateFullyQualifiedAttributeType(attribute, false);
                }
                String getterName = ("Boolean".equalsIgnoreCase(fieldClass) ? "is" : "get") + StringUtils.capitalize(fieldName);
                StringBuilder sb = new StringBuilder();
                sb.append("    public ").append(fieldClass).append(" ")
                        .append(getterName).append("() {\n    ").append("    return this.")
                        .append(fieldName).append(";\n    ").append("}");
                return CtMethod.make(sb.toString(), declaringClass);
            }
        }
        return null;
    }

    private static CtMethod generateSetter(CtClass declaringClass, Attributes attribute)
            throws CannotCompileException, ClassNotFoundException {
        String fieldName = attribute.getAttribute();
        String fieldClass = attribute.getType();
        if (!canonicalModelDecorator.isCanonicalReference(attribute.getType())) {
            if (fieldName != null) {
                fieldName = canonicalModelDecorator.decorateAttributeName(attribute);
                if (AttributesUtils.isList(fieldClass)) {
                    fieldClass = "java.util.List";
                } else {
                    fieldClass = canonicalModelDecorator.decorateFullyQualifiedAttributeType(attribute, false);
                }
                String setterName = "set" + StringUtils.capitalize(fieldName);
                StringBuilder sb = new StringBuilder();
                sb.append("    public void ").append(setterName).append("(")
                        .append(fieldClass).append(" ").append(fieldName)
                        .append(")").append(" {\n    ").append("    this.").append(fieldName)
                        .append("=").append(fieldName).append(";\n    ").append("}");
                return CtMethod.make(sb.toString(), declaringClass);
            }
        }
        return null;
    }

    private static void addGettersAndSetters(Collection<CanonicalModel> canonicalModels, CanonicalModel canonicalModel, CtClass cc) throws Exception {
        for (Attributes attribute : canonicalModel.getAttributes()) {
            if (!AttributesUtils.isAttributeAnEnumerated(canonicalModels, attribute)) {
                final CtMethod getterMethod = generateGetter(cc, attribute);
                if (getterMethod != null) {
                    cc.addMethod(getterMethod);
                }
                final CtMethod setterMethod = generateSetter(cc, attribute);
                if (setterMethod != null) {
                    cc.addMethod(setterMethod);
                }
            }
        }
    }


    private static void addField(CtClass cc, ClassPool pool, Attributes attribute) throws CannotCompileException, ClassNotFoundException {
        final String classname = canonicalModelDecorator.decorateFullyQualifiedAttributeType(attribute, true);
        if (!AttributesUtils.isInterface(attribute.getType())) {
            if (!AttributesUtils.isList(attribute.getType())) {
                if (pool.getOrNull(classname) == null) {
                    throw new CannotCompileException("Class '" + classname + "' from atribute '" + attribute.getAttribute()
                            + "' does not exist. Please create it first before trying to declare a new field");
                }
            } else {
                if (pool.getOrNull(AttributesUtils.getExtractedGenerics(classname)) == null) {
                    throw new CannotCompileException("Class 'List<" + classname + ">' from atribute '" + attribute.getAttribute()
                            + "' does not exist. Please create it first before trying to declare a new field");
                }
            }

            CtField f = CtField.make(new StringBuilder()
                            .append("\n")
                            .append("    private ")
                            .append(AttributesUtils.isList(attribute.getType()) ? "java.util.List" : classname)
                            .append(" ")
                            .append(canonicalModelDecorator.decorateAttributeName(attribute))
                            .append(";").toString(),
                    cc);
            cc.addField(f);
        }
    }

    private static void addRecursiveField(Collection<CanonicalModel> canonicalModels, CanonicalModel originalModelForAttribute, final Attributes attribute, String javaCanonicalModelSourcePackage, String javaCanonicalModelPath, String classesBasePath) throws Exception {
        CanonicalModel canonicalModel = (CanonicalModel) org.apache.commons.collections.CollectionUtils
                .find(canonicalModels, new Predicate() {
                    @Override
                    public boolean evaluate(Object object) {
                        return attribute.getType().equalsIgnoreCase(((CanonicalModel) object).getEntity());
                    }
                });
        boolean alreadyGenerated = false;
        if (canonicalModel == null) {
            if (canonicalModelDecorator.isCanonicalReference(attribute.getType())) {
                final String origin = canonicalModelDecorator.unDecorateCanonicalOrigin(attribute.getType());
                System.out.println("WARN: Attribute of type '" + attribute.getType() + "' uses an canonical entity from a foreign model of '" + origin + "'");
            } else {
                attribute.setType(canonicalModelDecorator.unDecorateAttributeType(attribute.getType()));
                if (AttributesUtils.isList(attribute.getType())) {
                    final String extractedGenerics = AttributesUtils.getExtractedGenerics(attribute.getType());
                    String type = AttributesUtils.getJavaTypeOrNull(extractedGenerics, false);
                    if (StringUtils.isNotBlank(type)) {
                        alreadyGenerated = true;
                    } else {
                        canonicalModel = (CanonicalModel) org.apache.commons.collections.CollectionUtils
                                .find(canonicalModels, new Predicate() {
                                    @Override
                                    public boolean evaluate(Object object) {
                                        return extractedGenerics.equalsIgnoreCase(((CanonicalModel) object).getEntity());
                                    }
                                });
                    }
                } else if (AttributesUtils.isInterface(attribute.getType())) {
                    canonicalModel = originalModelForAttribute;
                    canonicalModel.setInterface(true);
                } else {
                    throw new Exception("The type '" + attribute.getType() + "' declared for attribute '"
                            + attribute.getAttribute() + "' of entity '" + originalModelForAttribute.getEntity()
                            + "' is not found. All entities must be provided and each attribute type must match either any of them or a valid java class");
                }
            }
        }
        if (!alreadyGenerated) {
            if (canonicalModel != null) {
                generateJavaClass(canonicalModels, javaCanonicalModelSourcePackage, javaCanonicalModelPath, classesBasePath, canonicalModel);
            }
        }
    }


    private static DecompilerSettings javaDefaults() {
        final DecompilerSettings settings = new DecompilerSettings();
        settings.setFormattingOptions(JavaFormattingOptions.createDefault());
        return settings;
    }
}
