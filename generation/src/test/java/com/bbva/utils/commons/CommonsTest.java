package com.bbva.utils.commons;

import com.bbva.utils.google.drive.GoogleDriveUtils;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonMethod;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.File;
import java.io.IOException;

/**
 * Created by Alejandro on 22/12/2014.
 */
public class CommonsTest {

    protected File configFile;
    protected File configFileEspana;
    protected GoogleDriveUtils googleDriveUtils;
    protected ObjectMapper mapper;

    public void init() {
        mapper = new ObjectMapper();
        mapper.setVisibility(JsonMethod.FIELD, JsonAutoDetect.Visibility.ANY);
        mapper.setVisibility(JsonMethod.GETTER, JsonAutoDetect.Visibility.ANY);
        mapper.setVisibility(JsonMethod.SETTER, JsonAutoDetect.Visibility.ANY);
        mapper.configure(JsonGenerator.Feature.ESCAPE_NON_ASCII, false);
    }

    public void setUp() {
        init();
        try {
            googleDriveUtils = new GoogleDriveUtils();
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }

    protected String toJSON(final Object object) throws IOException {
        return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
    }
}
