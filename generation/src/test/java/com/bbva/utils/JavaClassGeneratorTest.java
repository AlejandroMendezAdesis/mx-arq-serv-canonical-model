package com.bbva.utils;

import com.bbva.utils.commons.CommonsTest;
import com.bbva.utils.templates.CanonicalModelToBean;
import com.bbva.utils.templates.model.canonical.CanonicalModels;
import org.apache.commons.collections.Closure;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Alejandro on 22/12/2014.
 */
public class JavaClassGeneratorTest extends CommonsTest {

    private static final String PROJECT_BASE_PATH = "C:\\workspace\\canonical-model\\src\\main\\java";

    @Test
    @Ignore
    public void testParseExcelFileToCanonicalModelFromDrive() throws IOException {
        setUp();
        final Collection<File> files = googleDriveUtils.downloadFilesFromDrive();
        final URL configUrl = this.getClass().getResource("/config.xml");
        configFile = new File(configUrl.getFile());
        final URL configUrlEspana = this.getClass().getResource("/configEspana.xml");
        configFileEspana = new File(configUrlEspana.getFile());

        org.apache.commons.collections.CollectionUtils.forAllDo(files, new Closure() {
            public void execute(final Object o) {
                final File excelFile = (File) o;
                final CanonicalModelToBean canonicalModelToBean = new CanonicalModelToBean(excelFile, configFile, configFileEspana);
                Collection<CanonicalModels> canonicalModelsList;
                try {
                    canonicalModelsList = canonicalModelToBean.parseExcelFile();
                    System.out.println("*********** CanonicalModel - " + excelFile.getName() + " ************** ");
                    System.out.println(toJSON(canonicalModelsList));
                    System.out.println("\n*********** Generating Java Classes ************** ");
                    File file = new File(PROJECT_BASE_PATH);
                    for (CanonicalModels canonicalModels : canonicalModelsList) {
                        JavaClassGenerator.generateJavaClassFromCanonicalModel(file, canonicalModels);
                    }
                } catch (final Throwable e) {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
                Assert.assertNotNull(canonicalModelsList);
            }
        });

    }


    @Test
    @Ignore
    public void testParseExcelFileToCanonicalModel() throws IOException {
        init();
        final Collection<File> files = new ArrayList<>();
        files.add(new File(URLDecoder.decode(this.getClass().getResource("/MCD Colombia v1.xlsx").getFile(), "UTF-8")));
        final URL configUrl = this.getClass().getResource("/config.xml");
        configFile = new File(configUrl.getFile());
        final URL configUrlEspana = this.getClass().getResource("/configEspana.xml");
        configFileEspana = new File(configUrlEspana.getFile());

        org.apache.commons.collections.CollectionUtils.forAllDo(files, new Closure() {
            public void execute(final Object o) {
                final File excelFile = (File) o;
                final CanonicalModelToBean canonicalModelToBean = new CanonicalModelToBean(excelFile, configFile, configFileEspana);
                Collection<CanonicalModels> canonicalModelsList;
                try {
                    canonicalModelsList = canonicalModelToBean.parseExcelFile();
                    System.out.println("*********** CanonicalModel - " + excelFile.getName() + " ************** ");
                    System.out.println(toJSON(canonicalModelsList));
                    System.out.println("\n*********** Generating Java Classes ************** ");
                    File file = new File(PROJECT_BASE_PATH);
                    for (CanonicalModels canonicalModels : canonicalModelsList) {
                        JavaClassGenerator.generateJavaClassFromCanonicalModel(file, canonicalModels);
                    }
                } catch (final Throwable e) {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
                Assert.assertNotNull(canonicalModelsList);
            }
        });

    }

}
