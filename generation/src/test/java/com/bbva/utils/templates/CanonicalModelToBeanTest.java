package com.bbva.utils.templates;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Collection;

import com.bbva.utils.commons.CommonsTest;
import com.bbva.utils.templates.model.canonical.CanonicalModels;
import org.apache.commons.collections.Closure;
import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Created by Alejandro on 20/11/2014.
 */
public class CanonicalModelToBeanTest extends CommonsTest {


	@Test
    @Ignore
	public void testParseExcelFileToCanonicalModelFromDrive() throws IOException {
		setUp();
		final Collection<File> files = googleDriveUtils.downloadFilesFromDrive();
		final URL configUrl = this.getClass().getResource("/config.xml");
		configFile = new File(configUrl.getFile());
		final URL configUrlEspana = this.getClass().getResource("/configEspana.xml");
		configFileEspana = new File(configUrlEspana.getFile());

		CollectionUtils.forAllDo(files, new Closure() {
			public void execute(final Object o) {
				final File excelFile = (File) o;
				final CanonicalModelToBean canonicalModelToBean = new CanonicalModelToBean(excelFile, configFile, configFileEspana);
				Collection<CanonicalModels> canonicalModelsList;
				try {
					canonicalModelsList = canonicalModelToBean.parseExcelFile();
					System.out.println("*********** CanonicalModel - " + excelFile.getName() + " ************** ");
					System.out.println(toJSON(canonicalModelsList));
				} catch (final Throwable e) {
					e.printStackTrace();
					throw new RuntimeException(e);
				}
				Assert.assertNotNull(canonicalModelsList);
			}
		});

	}




}
