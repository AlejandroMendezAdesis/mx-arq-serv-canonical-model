package com.bbva.utils.templates.model.xls;

/**
 * Created by Alejandro on 19/12/2014.
 */
public class Origin {
    private String countryName;

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryName() {
        return countryName;
    }
}
