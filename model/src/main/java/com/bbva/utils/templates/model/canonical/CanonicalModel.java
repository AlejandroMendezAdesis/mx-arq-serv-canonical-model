package com.bbva.utils.templates.model.canonical;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by Alejandro on 19/12/2014.
 */
public class CanonicalModel {

    private String entity;
    private Set<Attributes> attributes = new LinkedHashSet<Attributes>();
    private boolean isInterface = false;
    private boolean isEnumerate = false;
    private String extendsImplements;

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public Set<Attributes> getAttributes() {
        return attributes;
    }

    public void setAttributes(Set<Attributes> attributes) {
        this.attributes = attributes;
    }

    public boolean isInterface() {
        return isInterface;
    }

    public void setInterface(boolean isInterface) {
        this.isInterface = isInterface;
    }

    public boolean isEnumerate() {
        return isEnumerate;
    }

    public void setEnumerate(boolean isEnumerate) {
        this.isEnumerate = isEnumerate;
    }

    public String getExtendsImplements() {
        return extendsImplements;
    }

    public void setExtendsImplements(String extendsImplements) {
        this.extendsImplements = extendsImplements;
    }
}
