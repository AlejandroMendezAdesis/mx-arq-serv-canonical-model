package com.bbva.utils.templates.model.canonical;

import com.bbva.utils.templates.model.xls.Origin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by Alejandro on 19/12/2014.
 */
public class CanonicalModels {

    Origin origin;
    Set<CanonicalModel> models = new LinkedHashSet<CanonicalModel>();

    public Origin getOrigin() {
        return origin;
    }

    public void setOrigin(Origin origin) {
        this.origin = origin;
    }

    public Set<CanonicalModel> getModels() {
        return models;
    }

    public void setModels(Set<CanonicalModel> models) {
        this.models = models;
    }
}
