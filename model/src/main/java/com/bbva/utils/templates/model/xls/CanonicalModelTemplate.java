package com.bbva.utils.templates.model.xls;

/**
 * Created by Alejandro on 19/12/2014.
 */
public class CanonicalModelTemplate {

    String entity;
    String attribute;
    String format;
    String type;
    String extendsImplements;
    String description;

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getExtendsImplements() {
        return extendsImplements;
    }

    public void setExtendsImplements(String extendsImplements) {
        this.extendsImplements = extendsImplements;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

  }
