package com.bbva.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Alejandro on 03/12/2014.
 */
public class CollectionUtils {

    public static NotNullsLinkedHashSet wrapList(Collection<Object> collection) {
        return new NotNullsLinkedHashSet<Object>(collection);
    }

    public static List buildEmptyList(List collection) {
        return new ArrayList<Object>(collection);
    }

    public static List buildInitializedList(Object value) {
        final ArrayList<Object> arrayList = new ArrayList<Object>();
        arrayList.add(value);
        return arrayList;
    }

    public static List<Object> fillList(List<Object> collection, Object value) {
        collection.add(value);
        return collection;
    }
}
