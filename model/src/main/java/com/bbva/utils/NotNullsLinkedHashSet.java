package com.bbva.utils;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections.Predicate;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * Created by alejandro on 13/10/14.
 */

public class NotNullsLinkedHashSet<T> extends LinkedHashSet<T> {

	private static final long serialVersionUID = 4462897959138488249L;

	private boolean traverseFields;

	public NotNullsLinkedHashSet() {
		this(false);
	}

	public NotNullsLinkedHashSet(final boolean traverseFields) {

		super();
		this.traverseFields = traverseFields;
	}

	public NotNullsLinkedHashSet(final Collection<? extends T> elements) {
		super();
		this.addAll(elements);
	}

	@Override
	public boolean add(final T e) {
		return e != null && (!this.traverseFields || !allFieldsAreNull(e)) && super.add(e);
	}

	@SuppressWarnings("unchecked")
	private boolean allFieldsAreNull(final T e) {
		try {
			final Map<String, Object> map = PropertyUtils.describe(e);
			for (final Map.Entry<String, Object> entry : map.entrySet()) {
				if (!("class".equals(entry.getKey()) && Class.class.isAssignableFrom(entry.getValue().getClass()))
						&& entry.getValue() != null) {
					return false;
				}
			}
		} catch (final IllegalAccessException iae) {
			iae.printStackTrace();
		} catch (final InvocationTargetException ite) {
			ite.printStackTrace();
		} catch (final NoSuchMethodException nsme) {
			nsme.printStackTrace();
		}
		return true;
	}

	public int indexOf(final T e) {
		int result = 0;

		for (final T element : this) {
			if (element.equals(e)) {
				return result;
			}
			result++;
		}

		return -1;
	}


	public int indexOf(Predicate e) {
		int result = 0;

		for (final T element : this) {
			if (e.evaluate(element)) {
				return result;
			}
			result++;
		}

		return -1;
	}

	@Override
	public boolean addAll(final Collection<? extends T> elements) {
		boolean result = true;
		if (elements == null) {
			return false;
		}
		for (final T element : elements) {
			result = add(element) && result;
		}
		return result;
	}

}