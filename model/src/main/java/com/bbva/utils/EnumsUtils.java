package com.bbva.utils;

import org.apache.commons.collections.Closure;
import org.apache.commons.collections.CollectionUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Alejandro on 27/11/2014.
 */
public class EnumsUtils {

    public static List<String> getNamesList(Class enumerateClazz) {
        if (Enum.class.isAssignableFrom(enumerateClazz)) {
            final List<String> result = new ArrayList<String>();
            List enumConstants = Arrays.asList(enumerateClazz.getEnumConstants());
            CollectionUtils.forAllDo(enumConstants, new Closure() {
                @Override
                public void execute(Object o) {
                    result.add(((Enum) o).name());
                }
            });
            return result;

        } else {
            throw new IllegalArgumentException("Class is not an Enum type");
        }
    }


    public static List<String> getValuesList(Class enumerateClazz) {
        if (Enum.class.isAssignableFrom(enumerateClazz)) {
            final List<String> result = new ArrayList<String>();
            List enumConstants = Arrays.asList(enumerateClazz.getEnumConstants());
            CollectionUtils.forAllDo(enumConstants, new Closure() {
                @Override
                public void execute(Object o) {
                    try {
                        final Method value = o.getClass().getMethod("value");
                        java.security.AccessController.doPrivileged
                                (new java.security.PrivilegedAction() {
                                    public Object run() {
                                        value.setAccessible(true);
                                        return null;
                                    }
                                });
                        result.add((String) value.invoke(o));
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }

                }
            });
            return result;

        } else {
            throw new IllegalArgumentException("Class is not an Enum type");
        }
    }
}
